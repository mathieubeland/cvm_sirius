<?php
    require_once("action/menuAction.php");
    $action = new MenuAction();
    $action->execute();
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/menu.css">
    <script src="js/jquery.js"></script>    
    <script src="js/menuAnimation.js"></script>
    <script src="js/sprite/TiledImage.js"></script>
    <script src="js/sprite/Personnage.js"></script>
    <script src="js/sprite/Frogs.js"></script>
    <audio src="audio/menu.mp3" id="player"></audio>
    <title>Sirius</title>
</head>
<body>
    <canvas id="menu_canvas"></canvas>
    <div id="logOutBar" onclick="signout()" type="submit"><label>Déconnexion</label></div>
    <div id="user_info">
    </div>
    <script type="info_script" id="info_template">
        <div>
            <label>Nom:</label><p id="nom"></p>
        </div>
        <div>
            <label>HP: </label><div id="hp"></div>
        </div>
        <div>
            <label>MP: </label><div id="mp"></div>
        </div>
        <div>
            <label>Niveau: </label><div id="niveau"></div>
        </div>
        <div>
            <label>Experience: </label><div id="exp"></div>
        </div>
        <div>
            <label>Victoires: </label><div id="victoires"></div>
        </div>
        <div>
            <label>Défaites: </label><div id="defaites"></div>
        </div>
        <div>
            <label>Chances de feintes: </label><div id="feintes"></div>
        </div>
        <div>
            <label>Armure: </label><div id="armure"></div>
        </div>
    </script>
    <div id="board">
    </div>
</body>
</html>