<?php
    require_once("action/arenaAction.php");
    $action = new ArenaAction();
    $action->execute();
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/arena.css">
    <script src="js/arenaAnimation.js"></script>   
    <script src="js/sprite/TiledImage.js"></script>     
    <script src="js/sprite/Personnage.js"></script>
    <script src="js/sprite/Naruto.js"></script>
    <script src="js/sprite/Sasuke.js"></script>
    <script src="js/sprite/Jiraiya.js"></script>  
    <script src="js/sprite/Kakashi.js"></script>     
    <script src="js/sprite/fukaShima.js"></script>                                
    <script src="js/sprite/chakraBlast.js"></script>    
    <script src="js/sprite/Rasengan.js"></script> 
    <script src="js/sprite/BigRasengan.js"></script>     
    <script src="js/sprite/Katon.js"></script>
    <script src="js/sprite/Fume.js"></script>   
    <script src="js/sprite/EndGame.js"></script>       
    <script src="js/sprite/Sharingan.js"></script>
    <script src="js/jquery.js"></script>
    <audio src="audio/arena.mp3" id="player"></audio>
    <audio src="audio/rasengan.wav" id="rasengan"></audio>   
    <audio src="audio/tebayo.wav" id="tebayo"></audio>   
    <audio src="audio/chakra.wav" id="chakra"></audio> 
    <audio src="audio/katon.wav" id="katon"></audio>
    <audio src="audio/jirayia.wav" id="jirayia"></audio>                                         
    <audio src="audio/sharingan.wav" id="sharingan"></audio>                                         
    <audio src="audio/fuka.wav" id="fuka"></audio>                                                                                      
    <title>Sirius</title>
</head>
<body>
    <div id="conteneur">
        <canvas id="canvasArena" width="1200" height="800"></canvas>
    </div>
    <div id="gameElements">
        <div id="game_name"></div>
        <div id="players_infos">
            <img src="images/chakraIcon.png" alt="chakra">
            <div id="mp_hp_bar" class="bar">
                <div><div id="hp" class="hp"></div></div>
                <div><div id="mp" class="mp"></div></div>
            </div>
            <div id="buffer"></div>
            <div id="allies_bars"></div>
        </div>
        <div id="boss_life" class="bar">
            <div><div id="boss_hp" class="hp"></div></div>
        </div>
        <div id="attacks">
            <div id="attaque1" class="anAttack" onclick="att1()"></div>
            <div id="attaque2" class="anAttack" onclick="spec1()"></div>
            <div id="attaque3" class="anAttack" onclick="spec2()"></div>
        </div>
    </div>
    <script type="attack_script" id="attack_template">
        <div class='attack_name'></div>
        <div class='second_line'>
            <div class='attack_mp'></div>
            <div class='attack_dmg'></div>
            <div class='attack_heal'></div>
        </div>
    </script>
    <script type="allie_bar_script" id="allie_template">
        <div class="allie_bar">
            <div class="back"><div class="hp"></div></div>
        </div>
	</script>
</body>
</html>