<?php
    require_once("action/CommonAction.php");
    require_once("action/DAO/GameDAO.php");	

    class AjaxMenuAction extends CommonAction {
        public $result = null;

        public function __construct() {
            parent::__construct(CommonAction::$VISIBILITY_MEMBER);
        }

        protected function executeAction() {
            if (isset($_POST["deconnexion"])) {
                $this->result = GameDAO::signout();
                if($this->result == "USER_SIGNED_OUT"){
                    session_unset();
                    session_destroy();
                }
            } else if (isset($_POST["user_info"])) {          
                $this->result = GameDAO::user_info();
            } else if (isset($_POST["list"])) {          
                $this->result = GameDAO::list_games();
            } else if (isset($_POST["id"])) {
                $this->result = GameDAO::join_game($_POST["id"]);
            }
        }
    }
