<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/GameDAO.php");	

	class IndexAction extends CommonAction {
		public $wrongLogin = false;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$this->wrongLogin = false;
			if(!empty($_POST["username"]) && !empty($_POST["password"])){
				$key = GameDAO::signin($_POST["username"], $_POST["password"]);
				if (strlen ($key) === 40) {
					$_SESSION["key"] = $key;
					$_SESSION["visibility"] = CommonAction::$VISIBILITY_MEMBER;
					$_SESSION["username"] = $_POST["username"];
					header("location:menu.php");
					exit;
				}
				else {
					$this->wrongLogin = true;
				}
			} else if(!empty($_POST["username"]) || !empty($_POST["password"])){
				$this->wrongLogin = true;
			}
		}
	}