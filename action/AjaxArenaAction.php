<?php
    require_once("action/CommonAction.php");
    require_once("action/DAO/GameDAO.php");	

    class AjaxArenaAction extends CommonAction {
        public $result = null;

        public function __construct() {
            parent::__construct(CommonAction::$VISIBILITY_MEMBER);
        }

        protected function executeAction() {
            if (isset($_POST["get_state"])) {
                $this->result = GameDAO::state();
            } else if (isset($_POST["attack_name"])) {
                $this->result = GameDAO::attack($_POST["attack_name"]);
            }
        }
    }
