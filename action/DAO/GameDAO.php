<?php
    require_once("action/CommonAction.php");

	class GameDAO {
        public static function callAPI($service, array $data) {
            $apiURL = "https://apps-de-cours.com/web-sirius/server/api/" . $service . ".php";

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($apiURL, false, $context);

            if (strpos($result, "<br") !== false) {
                    var_dump($result);
                    exit;
                }
                
            return json_decode($result);
        }

        public static function signin($username, $pwd){
            $data = [];
            $data["username"] = $username;
            $data["pwd"] = $pwd;
            $key = GameDAO::callAPI("signin", $data);
            return $key;
        }

        public static function signout(){
            $data = [];
            $data['key'] = $_SESSION["key"];

            $result = GameDAO::callAPI("signout", $data);
            return $result;
        }

        public static function user_info(){
            $data = [];
            $data['key'] = $_SESSION["key"];
            $result = GameDAO::callAPI("user-info", $data);
            return $result;
        }

        public static function list_games(){
            $data = [];
            $data['key'] = $_SESSION["key"];
            $result = GameDAO::callAPI("list", $data);
            return $result;
        }

        public static function join_game($id){
            $data = [];
            $data['key'] = $_SESSION["key"];
            $data['id'] = $id;            
            $result = GameDAO::callAPI("enter", $data);
            return $result;
        }

        public static function state(){
            $data = [];
            $data['key'] = $_SESSION["key"];       
            $result = GameDAO::callAPI("state", $data);
            return $result;
        }

        public static function attack($name){
            $data = [];
            $data['key'] = $_SESSION["key"];
            $data['skill-name'] = $name;
            $result = GameDAO::callAPI("action", $data);
            return $result;
        }
	}

