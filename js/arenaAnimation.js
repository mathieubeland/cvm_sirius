let canvas = null;
let canvasOriginalWidth = 1024;
let canvasOriginalHeight = 576;
let ctx = null;

let spriteList = [];
let spriteScale = 1.5;
let spriteVerticalPosition = null;
let refreshDelay = 100;

//Variables relatives à Naruto
let naruto = null;
let positionNaruto = null;
let attack1 = false;            
let attack2 = false;            
let attack3 = false;
let attack1Enabled = false;            
let attack2Enabled = false;            
let attack3Enabled = false;
let attackTimeElapsed = true;

//Variables relatives à Sasuke
let sasuke = null;
let positionSasuke = null;
let attaqueSasuke = false;

//Variables relatives à Jiraiya
let jiraiya = null;
let attaqueJiraiya = false;
let positionHorizontaleJiraya = null;
let positionVerticaleJiraya = null;

//Variables relatives à Jiraiya
let fukasakuShima = null;
let positionHorizontaleFukasakuShima = null;
let positionVerticaleFukasakuShima = null;
let attaqueFukasakuShima = false;

//Variables relatives à Jiraiya
let kakashi = null;
let positionHorizontaleKakashi = null;
let positionVerticaleKakashi= null;
let attaqueKakashi = false;

//Pour savoir si les attaques ont été mises à jour
//Elles ne sont mises à jour qu'une seule fois en début de partie
let permanentSetted = false;

window.onload = () => {
	canvas = document.getElementById("canvasArena");
	ctx = canvas.getContext("2d");
	spriteVerticalPosition = canvas.height - 100;

	positionSasuke = canvas.width * 0.77;
	sasuke = new Sasuke();
	spriteList.push(sasuke);	

	positionNaruto = canvas.width * 0.22;
	naruto = new Naruto();
	spriteList.push(naruto);

	//Pour faire jouer la musique
	let player = document.getElementById('player');
	player.play();

	setTimeout(showGameElements, 2000);
	setTimeout(state, 2000);
	setTimeout(generalTick, 2000);
}

function showGameElements(){
	document.getElementById("gameElements").style.visibility = "visible";
}

function generalTick(){
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	for (let i = 0; i < spriteList.length; i++) {
		const element = spriteList[i];
		const alive = element.tick();
		if(!alive){
			spriteList.splice(i, 1);
			i--;
		}
	}
	window.requestAnimationFrame(generalTick);
}

function state(){
	$.ajax({
        type : "POST",
        url : "ajaxArena.php",
        data : {
            get_state : true
        }
    })
    .done(response => {
		response = JSON.parse(response);
		if(typeof response !== "object"){
			if(response == "GAME_NOT_FOUND_WIN"){
				spriteList.push(new EndGame(true));
				document.getElementById("boss_hp").style.width = 0;
			}else if(response == "GAME_NOT_FOUND_LOST"){
				spriteList.push(new EndGame(false));
				document.getElementById("hp").style.width = 0;
				attaqueSasuke = true;
			} else {
				console.log(response);
			}
			setTimeout(backToMenu, 5000);
		} else {
			updatesPlayers(response);
			if(!permanentSetted){
				fillPermanentInfos(response);
			}
		}
		setTimeout(state, 2000);
    });
}

function backToMenu(){
	window.location= "menu.php";
}

//Personnagers secondaires----------------------------------------------------------a
function createKakashi(name, hp, max){
	positionHorizontaleKakashi = canvas.width * 0.05;
	positionVerticaleKakashi = spriteVerticalPosition;
	kakashi = new Kakashi(name, hp, max);
	spriteList.push(kakashi);
}

function createJiraya(name, hp, max){
	positionHorizontaleJiraya = canvas.width * 0.15;
	positionVerticaleJiraya = spriteVerticalPosition + 35;
	jiraiya = new Jiraiya(name, hp, max);
	spriteList.push(jiraiya);
}

function createFukasakuShima(name, hp, max){
	positionHorizontaleFukasakuShima = canvas.width * 0.12;
	positionVerticaleFukasakuShima = spriteVerticalPosition - 40;
	fukasakuShima = new FukaShima(name, hp, max);
	spriteList.splice(0,0, fukasakuShima);
}
// ---------------------------------------------------------------------------------a

function updatesPlayers(response){
	let hp = 100 * response.player.hp/response.player.max_hp;
	document.getElementById("hp").style.width = hp + "%";
	let mp = 100 * response.player.mp/response.player.max_mp;
	document.getElementById("mp").style.width = mp + "%";

	let boss_hp = 100 * response.game.hp/response.game.max_hp;
	document.getElementById("boss_hp").style.width = boss_hp + "%";

	
	//On compte le nombre d'alliers resprésentés à l'écran pour pouvoir comparer avec
	//Le nombre d'alliers renvoyé par le serveur.
	let numberOfAllies = 0;

	//On compte le nombre d'alliers représentés à l'écran
	if(kakashi){
		++numberOfAllies;
	}
	if(jiraiya){
		++numberOfAllies;	
	}
	if(fukasakuShima){
		++numberOfAllies;
	}

	//S'il y a moins de personnages que je joueurs, on crée de nouveaux personnages
	if(response.other_players.length > numberOfAllies && response.other_players.length < 4){
		for (const iterator of response.other_players) {
			if(iterator){
				let created = false;
				//On vérifie d'abord si le nom du joueur se retrouve déjà parmis les 
				//personnages créés
				if(kakashi && !created){
					if(kakashi.name == iterator.name){
						created = true;
					}
				}
				if(jiraiya && !created){
					if(jiraiya.name == iterator.name){
						created = true;
					}
				}
				if(fukasakuShima && !created){
					if(fukasakuShima.name == iterator.name){
						created = true;
					}
				}
				
				if(!kakashi && !created){ //S'il ne s'y trouve pas, on crée le joueur
					createKakashi(iterator.name, iterator.hp, iterator.max_hp);
					created = true;
				} else if(!jiraiya && !created){
					createJiraya(iterator.name, iterator.hp, iterator.max_hp);
					created = true;						
				} else if(!fukasakuShima && !created){
					createFukasakuShima(iterator.name, iterator.hp, iterator.max_hp);
					created = true;						
				}
			}
		}
	}

	//Chaque personnage recherche dans la lise de noms si le sien se retrouve dans la liste.
	//Si ce n'est pas le cas, il se retire lui-même de la liste.
	if(kakashi){
		kakashi.isAlive(response.other_players);
	}
	if(jiraiya){
		jiraiya.isAlive(response.other_players);	
	}
	if(fukasakuShima){
		fukasakuShima.isAlive(response.other_players);
	}
	
	for (const iterator of response.other_players) {
		//Permet de voir si les ennemis ont attaqué et de faire attaqué le bon personnage
		if(iterator.attacked != "--"){
			if(kakashi && kakashi.name == iterator.name){
				attaqueKakashi = true;
			} else if(jiraiya && jiraiya.name == iterator.name){
				attaqueJiraiya = true;
			} else if(fukasakuShima && fukasakuShima.name == iterator.name){
				attaqueFukasakuShima = true;				
			}
		}
		//On met à jour les barres de vie des personnages
		if(kakashi){
			if(kakashi.name == iterator.name){
				kakashi.drawLifeBar(iterator.hp, iterator.max_hp);
			}
		}
		if(jiraiya){
			if(jiraiya.name == iterator.name){
				jiraiya.drawLifeBar(iterator.hp, iterator.max_hp);
			}
		}	
		if(fukasakuShima){
			if(fukasakuShima.name == iterator.name){
				fukasakuShima.drawLifeBar(iterator.hp, iterator.max_hp);
			}
		}
	}

	//Va faire attaquer le boss
	if(response.game.attacked == true){
		attaqueSasuke = true;
	}
}

//Initialise les éléments html qui sont permanents lors d'une partie
function fillPermanentInfos(response){
	for (const iterator of response.player.skills) {
		let template = document.querySelector("#attack_template").innerHTML;
		let id =""
		if(iterator.name == "Normal"){
			id = "attaque1";
			attack1Enabled = true;
		}else if(iterator.name == "Special1"){
			id = "attaque2";
			attack2Enabled = true;
		}else if(iterator.name == "Special2"){
			id = "attaque3";
			attack3Enabled = true;
		}		
		let node = document.getElementById(id);
		node.innerHTML = template;
		node.querySelector(".attack_name").innerHTML = iterator.name;
		node.querySelector(".attack_mp").innerHTML = "Mp: " + iterator.cost;
		node.querySelector(".attack_dmg").innerHTML = "Dmg: " + iterator.dmg;
		node.querySelector(".attack_heal").innerHTML = "Heal: " + iterator.heal;
		node.style.opacity = 1.0;
	}
	document.getElementById("game_name").innerHTML = response.game.name + ": " + response.game.type;
	permanentSetted = true;
}

function att1(){
	if(attack1Enabled && attackTimeElapsed){
		attack("Normal");
		document.getElementById("attaque1").style.opacity = 0.3;
	}
}

function spec1(){
	if(attack2Enabled && attackTimeElapsed){
		attack("Special1");
		document.getElementById("attaque2").style.opacity = 0.3;
	}
}

function spec2(){
	if(attack3Enabled && attackTimeElapsed){
		document.getElementById("attaque3").style.opacity = 0.3;
		attack("Special2");
	}
}

//Permet de gérer le cooldown. 
function elapsed(node){
	if(attack1Enabled){
		document.getElementById("attaque1").style.opacity = 1.0;
	}
	if(attack2Enabled){
		document.getElementById("attaque2").style.opacity = 1.0;
	}
	if(attack3Enabled){
		document.getElementById("attaque3").style.opacity = 1.0;
	}
	attackTimeElapsed = true;
}

function attack(name){
	if(attackTimeElapsed){
		attackTimeElapsed = false;
		$.ajax({
			type : "POST",
			url : "ajaxArena.php",
			data : {
				attack_name: name
			}
		})
		.done(response => {
			response = JSON.parse(response);
			if(typeof response !== "object"){
				if(response == "OK"){
					if(name == "Normal"){
						attack1 = true;
					}else if(name == "Special1"){
						attack2 = true;
					}else if(name == "Special2"){
						attack3 = true;
					}
				} else {
					console.log(response);
				}
				setTimeout(elapsed, 2000);
			}
		});
	}
}