let canvas = null;
let ctx = null;

let arme1 = null;
let arme2 = null;

let imagesWidth = 100;
let ptRencontre = {
    "x":null,
    "y":null
};
let explosionImg = new Image();
explosionImg.src = "images/explosion.png";
explosionImg.position
let generator = null;

let height = 800;
let width = 2000;
window.onload = () => {
    canvas = document.getElementById("canvas");
    ctx = canvas.getContext("2d");
    ctx.canvas.width  = window.innerWidth;
    ctx.canvas.height = window.innerHeight;
    generator = new ProjectilesGenerator();

    //Pour faire jouer la musique
    let player = document.getElementById('player');
    player.play();
    
    getUserName();
    generalTick();
}

function generalTick() {
    ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);
    generator.tick();
    window.requestAnimationFrame(generalTick);
}

function getUserName(){
    if (localStorage["username"] != null) {
        document.getElementById("username").value = localStorage["username"];
    }
}

function saveUserName() {
    if(document.getElementById("username").value){
        localStorage["username"] = document.getElementById("username").value;
    }
}