let canvas = null;
let canvasOriginalWidth = 1024;
let canvasOriginalHeight = 576;
let ctx = null;

let spriteList = [];
let spriteScale = 1.5;
let spriteVerticalPosition = null;
let refreshDelay = 300;
let numberOfFrogs = 12;
let water = null;
let initialYSpeed = 3;

let leftMountainXMin = null;
let leftMountainXMax = null;
let leftMountainTop = null;

let rightMountainXMin = null;
let rightMountainXMax = null;
let rightMountainTop= null;

//Permet de savoir si le paneau contenant les infos du joeur a été créer à l'aide du template. 
let userInfos = null;

let gamesBoard = null;

window.onload = () => {
    gamesBoard = document.getElementById("board")
	canvas = document.getElementById("menu_canvas");
    ctx = canvas.getContext("2d");
    ctx.canvas.width  = window.innerWidth;
    ctx.canvas.height = window.innerHeight;
    water = ctx.canvas.height * 0.90;

    leftMountainXMin = 0;
    leftMountainXMax = 0.18 * ctx.canvas.width;
    leftMountainTop = 0.20 * ctx.canvas.height;

    rightMountainXMin = 0.80 * ctx.canvas.width;
    rightMountainXMax = ctx.canvas.width;
    rightMountainTop = 0.20 * ctx.canvas.height;

    //Faire jouer la musique
    let player = document.getElementById('player');
    player.play();

    for (let i = 0; i < numberOfFrogs; i++) {
        spriteList.push(new Frogs(i));
    }
    setTimeout(user_info, 2000);
    setTimeout(game_listing, 2000);
    generalTick();
}

function game_listing(){
    $.ajax({
        type : "POST",
        url : "ajaxMenu.php",
        data : {
            list : true
        }
    })
    .done(response => {
        response = JSON.parse(response);
        if(typeof response == "object"){
            $( "#board" ).empty();
            for (const iterator of response) {
                createGamePanel(iterator.name, iterator.level, iterator.nb, iterator.max_users, iterator.hp, iterator.type, iterator.exp, iterator.id);
            }
		}
        setTimeout(game_listing, 2000);
    });
}

function join_game(id){
    $.ajax({
        type : "POST",
        url : "ajaxMenu.php",
        data : {
            id : id
        }
    })
    .done(response => {
        response = JSON.parse(response);
        if(response == "GAME_ENTERED"){
            window.location= "arena.php";
        }
    });
}

//Permet de créer chacun des panels de la liste de parties
function createGamePanel(name, level, nbUsers, max_users, hp, type, exp, id){
    let container = document.createElement("div");
    container.setAttribute("class", "game");
    container.setAttribute("id", id);
    container.onclick = () => {
        join_game(id);
    }
    
    let lbl_name = document.createElement("label");
    let lbl_level = document.createElement("label");
    let lbl_players = document.createElement("label");
    let lbl_hp = document.createElement("label");
    let lbl_type = document.createElement("label");
    let lbl_exp = document.createElement("label");

    let txt_name = document.createTextNode(name);
    let txt_level = document.createTextNode("Niveau " + level);
    let txt_players = document.createTextNode(nbUsers + "/" + max_users);
    let txt_hp = document.createTextNode(hp + "HP");
    let txt_max_users = document.createTextNode(type);
    let txt_exp = document.createTextNode(exp + " xp");

    lbl_name.appendChild(txt_name);
    lbl_level.appendChild(txt_level);
    lbl_players.appendChild(txt_players);
    lbl_hp.appendChild(txt_hp);
    lbl_type.appendChild(txt_max_users);
    lbl_exp.appendChild(txt_exp);

    container.appendChild(lbl_name);
    container.appendChild(lbl_level);
    container.appendChild(lbl_players);
    container.appendChild(lbl_hp);
    container.appendChild(lbl_type);
    container.appendChild(lbl_exp);

    gamesBoard.appendChild(container);
    gamesBoard.style.visibility = "visible";
}

function user_info(){
    $.ajax({
        type : "POST",
        url : "ajaxMenu.php",
        data : {
            user_info : true
        }
    })
    .done(response => {
        response = JSON.parse(response);
        if(typeof response == "object"){
            if(!userInfos){
                let template = document.querySelector("#info_template").innerHTML;
                let node = document.getElementById("user_info");
                node.innerHTML = template;
                node.style.visibility = "visible";
                user_info = true;
            }
            document.getElementById("nom").innerHTML = response.username;
            document.getElementById("hp").innerHTML = response.hp;
            document.getElementById("mp").innerHTML = response.mp;        
            document.getElementById("niveau").innerHTML = response.level;
            document.getElementById("exp").innerHTML = response.exp + " (" + parseFloat(response.exp/response.next_level_exp * 100).toFixed(2) + "%)";
            document.getElementById("victoires").innerHTML = response.victories;
            document.getElementById("defaites").innerHTML = response.loss;
            document.getElementById("feintes").innerHTML = response.dodge_chance + " %";
            document.getElementById("armure").innerHTML = response.dmg_red + " %";
		}
        setTimeout(user_info, 2000);
    });
}

function generalTick(){
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	for (let i = 0; i < spriteList.length; i++) {
		const element = spriteList[i];
		const alive = element.tick();
		if(!alive){
			spriteList.splice(i, 1);
			i--;
		}
	}
	window.requestAnimationFrame(generalTick);
}

function signout(){
    $.ajax({
        type : "POST",
        url : "ajaxMenu.php",
        data : {
            deconnexion : true
        }
    })
    .done(response => {
        response = JSON.parse(response);
        window.location= "index.php";
    });
}

