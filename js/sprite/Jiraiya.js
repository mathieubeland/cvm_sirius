class Jiraiya extends Personnage{
	constructor(name, hp, max){
		super((positionHorizontaleJiraya), (positionVerticaleJiraya), 140, 100, name, hp, max);
		this.name = name;
		this.columnCount = 10;
		this.rowCount = 4;
		this.refreshDelay = refreshDelay; 
		this.loopColumns = true; 
        this.scale = spriteScale;
		this.sprite = new TiledImage("images/jiraiya.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);        
        this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 4); 
		this.sprite.setNextCycle(0, 0, 4, 0)
		this.alive = true;

		this.isAttacking = false;
		this.isMovingForward = false;
		this.isKicking = false;
		this.isComingBack = false;
		this.isAppearing = true;
	}

	stance(){
		this.sprite.setFlipped(false);
        this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 4); 
	}

	attaque(){
		attaqueJiraiya = false;
		this.sprite.changeRow(2);
		this.sprite.changeMinMaxInterval(0, 6);
		this.sprite.resetCol();
		this.sprite.resetAfterLoop(true);
		spriteList.push(new BigRasengan(this.x, this.y -20));
		document.getElementById('jirayia').play();
		this.actionProcess();
	}

	apparition(){
		this.sprite.changeRow(3);
		this.sprite.changeMinMaxInterval(0, 2);
		if(this.sprite.getFrameCount() > 6){
			this.isAppearing = false;
		}
	}
	
	actionProcess(){		
		if(!this.isAttacking){
			this.isAttacking = true;
		}		
		if(!this.sprite.isLooping()){
			this.isAttacking = false;
		}
	}

	isAlive(players){
		let alive = false;
		for(const iterator of players){		
			if(iterator.name == this.name){
				alive = true;
			}
		}
		if(alive == false){
			jiraiya = null;
		}
		this.alive = alive;
	}

	tick(){
		super.drawLifeBar(this.hp, this.max);
		if(attaqueJiraiya){
			this.attaque();
		} else if(this.isAttacking){
			this.actionProcess();	
		} else if(this.isAppearing){
			this.apparition();	
		} else {
			this.stance();
		}
		this.sprite.tick(this.x, this.y, ctx);
		return this.alive;
	}
}