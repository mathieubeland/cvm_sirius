//La fumée de la troisième attaque de Naruto
class Fume{
	constructor(){
		this.x = sasuke.x - 95;
        this.y = spriteVerticalPosition - 120;
        this.img = new Image();
        this.img.src = "images/fume.png";
        this.size = 200;
        this.opacity = 1.0;
        this.growth = 2;
        this.opacityVariation = 0.01
	}

	tick(){
        let alive = true;
        this.opacity -= this.opacityVariation;
        this.x -= this.growth/2;
        this.y -= this.growth;
        this.size += this.growth;

        ctx.globalAlpha = this.opacity;
        ctx.drawImage(this.img, this.x, this.y, this.size, this.size);
        ctx.globalAlpha = 1.0;
        if(this.opacity<=0+this.opacityVariation){
            alive = false;
        }
		return alive;
	}
}