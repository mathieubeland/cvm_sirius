class Frogs extends Personnage{
	constructor(id){
        super(0, 0, 75, 75);
        this.id = id;
		this.columnCount = 5;
		this.rowCount = numberOfFrogs;
        this.refreshDelay = refreshDelay; 
		this.loopColumns = true; 
        this.scale = spriteScale;
		this.sprite = new TiledImage("images/frogsSprite.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);        
        this.sprite.changeRow(id);
		this.sprite.changeMinMaxInterval(1, 4); 
        this.sprite.setNextCycle(this.id, 0, 0, 0);
        if(Math.random() > 0.5){
            this.leftMountain = true;
            this.x = Math.random() * leftMountainXMax;
            this.y = ctx.canvas.height - 30
        }else{
            this.leftMountain = false;
            this.x = (Math.random() * (rightMountainXMax - rightMountainXMin)) + rightMountainXMin - this.width;
            this.y = ctx.canvas.height - 30
        }
        if(Math.random() > 0.5){
            this.left = true;
        }else{
            this.left = false;
        }
		this.isMoving = false;
        this.isFalling = false;
        this.isStanding = false;
        this.isInAction = false;
        this.idDiving = false;
        this.speedX = 3;
        this.speedY = initialYSpeed;
        this.downscaling = 0.005;
    }
    
    move(){
        this.sprite.changeMinMaxInterval(1, 4);
        if((this.sprite.getCurrentCol() % 3) == 0){
            if(Math.random() > 0.7){
                this.isMoving = false;
                this.isStanding = true;
            }
		}else if(Math.random() > 0.2){
            let x = Math.random() * 6;
            let y = Math.random() * 6;
            //Les grenouilles montent
            if(this.leftMountain){
                if(this.y < leftMountainTop){
                    this.isFalling = true;
                } else {
                    this.y -= y;
                }
            } else {
                if(this.y < rightMountainTop){
                    this.isFalling = true;
                } else {
                    this.y -= y;
                }
            }
            if(this.isMoving){
                //Détermine de quel bord la grenouille va allé
                if(this.left){
                    if(this.leftMountain){
                        if(this.x + x < leftMountainXMax){
                            this.x += x;
                        } else {
                            this.changeSide();
                            this.x -= x;
                        }
                    } else {
                        if(this.x + x < rightMountainXMax){
                            this.x += x;
                        }else{
                            this.changeSide();
                            this.x -= x;
                        }
                    }
                } else {
                    if(this.leftMountain){
                        if(this.x - x > leftMountainXMin){
                            this.x -= x;
                        }else{
                            this.changeSide();
                            this.x += x;
                        }
                    } else {
                        if(this.x - x > rightMountainXMin){
                            this.x -= x;
                        }else{
                            this.changeSide();
                            this.x += x;
                        }
                    }
                }
            }
        }
    }

    relocate(){
        if(Math.random() > 0.5){
            this.leftMountain = true;
            this.x = Math.random() * leftMountainXMax;
            this.y = ctx.canvas.height + 50;
        }else{
            this.leftMountain = false;
            this.x = (Math.random() * (rightMountainXMax - rightMountainXMin)) + rightMountainXMin - this.width;
            this.y = ctx.canvas.height + 50;
        }
        this.speedY = initialYSpeed;
        this.scale = spriteScale;
        this.sprite.setScale(this.scale);        
    }

    changeSide(){
        if(Math.random() > 0.5){
            if(this.left){
                this.left = false;

            } else{
                this.left = true;
            }
        }
    }

    fall(){
        this.isFalling = true;
        if(this.leftMountain){
            this.left = true;
        } else {
            this.left = false;
        }
        this.sprite.changeMinMaxInterval(2,2);
        let temporaryYspeed = 0.5;
        if(this.leftMountain){
            let target = leftMountainXMax + (ctx.canvas.width/2 - leftMountainXMax)/2;
            if(this.x < target){
                this.x += this.speedX;
                this.y -= temporaryYspeed;
            } else {
                this.isFalling = false;                
                this.dive();
            }
        } else {
            let target = ctx.canvas.width/2 + (rightMountainXMin - ctx.canvas.width/2)/2;
            if(this.x > target){
                this.x -= this.speedX;
                this.y -= temporaryYspeed;
            } else {
                this.isFalling = false;                                
                this.dive();
            }
        }
        this.sprite.setScale(this.scale -= this.downscaling);
    }

    dive(){
        this.isDiving = true;
        this.sprite.changeMinMaxInterval(3,3);
        if(this.leftMountain){
            if(this.x < ctx.canvas.width/2){
                this.x += this.speedX;
            }
        } else {
            if(this.x > ctx.canvas.width/2){
                this.x -= this.speedX;
            }
        }
        if(this.y < water){
            this.speedY += 0.03;
            this.y += this.speedY;
        }
        if(this.scale - this.downscaling > 0){
            this.sprite.setScale(this.scale -= this.downscaling);
        } 
        
        if(this.y >= water || this.scale - this.downscaling <= 0){
            this.isDiving = false;
            this.relocate();
        }
    }

    stand(){
        this.sprite.changeMinMaxInterval(0, 0);
        this.isStanding = false;
    }

	tick(){
        let alive = true;
        super.tick();
        this.sprite.setFlipped(!this.left);
        if(this.isMoving){
            this.move();
		} else if(this.isFalling) {
			this.fall();
		} else if(this.isStanding){
            this.stand();
        } else if(this.isInAction){
            this.actionProcess();
        } else if(this.isDiving){
            this.dive();
        } else {
            let dice = Math.random();
            if(dice < 0.1){
                this.isMoving = true;
            }else if(dice < 1.0){
                this.isStanding = true;
            }
        }
        this.sprite.tick(this.x, this.y, ctx);
		return alive;
	}
}