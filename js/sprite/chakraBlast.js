//3e attaque de Naruto

class ChakraBlast{
	constructor(x, y){
		this.x = x;
		this.y = y;
		this.speedX = 5;
		this.columnCount = 10;
		this.rowCount = 2;
		this.refreshDelay = refreshDelay; 
		this.loopColumns = true; 
		this.scale = spriteScale;
		this.sprite = new TiledImage("images/chakraBall.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);
		this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 7);
		this.sprite.resetCol();
	}

	tick(){
		let alive = true;
		let frame = this.sprite.getFrameCount();
		if(frame >= 7){
			this.sprite.changeMinMaxInterval(8, 8);
			this.x += this.speedX;
		}

		if(this.x + 10 >= sasuke.x){
			this.sprite.changeMinMaxInterval(9, 9);
			spriteList.push(new Fume());
		}

		if(this.x >= sasuke.x){
			alive = false;
		}

		this.sprite.tick(this.x, this.y, ctx);
		return alive;
	}
}