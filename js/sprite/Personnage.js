class Personnage{
	constructor(x, y, width, height, name, hp, max){
		this.x = x;
		this.y = y;
		this.speedX = 8;
		this.speedY = 5;
		this.width = width;		
		this.height = height;
		this.name = name;
		this.hp = hp;
		this.max = max;
		this.right = true;
	}

	drawLifeBar(hp, max){
		this.hp = hp;
		this.max = max;

		let w = 150;
		let h =  20;

		let x = this.x - this.width/2;
		let y = this.y - 1.5 * this.height;

		//Dessiner le contour de la barre de vie
		ctx.strokeStyle = "rgb(0,0,0)";
        ctx.beginPath()
        ctx.rect(x, y, w, h);
        ctx.stroke();
		ctx.closePath();

		//Dessiner la vie
		ctx.fillStyle = "rgba(255, 50, 50, 0.8)";
		ctx.fillRect(x, y, 150 * this.hp/this.max, h);
		
		//Dessiner le nom du joueur
		ctx.fillStyle = "#dad730";
		ctx.font = "16px Arial";
		ctx.fillText(this.name, x + 10, y+h-5);
	}

	tick(){
	}
}