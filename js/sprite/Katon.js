class Katon{
	constructor(x, y){
		this.x = x;
		this.y = y;
		this.columnCount = 10;
		this.rowCount = 1;
		this.refreshDelay = refreshDelay/2; 
		this.loopColumns = true; 
		this.scale = spriteScale;
		this.sprite = new TiledImage("images/katon.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);
		this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 8);
		this.sprite.setNextCycle(0, 0, 8, 0);
		this.sprite.setFlipped(true);
		this.sprite.resetCol();
		this.sprite.resetAfterLoop(true);
	}

	tick(){
		let alive = true;
		if(!this.sprite.isLooping()){
			alive = false;
		}
		this.sprite.tick(this.x, this.y, ctx);
		return alive;
	}
}