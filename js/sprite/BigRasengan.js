//Attaque de Jiraiya

class BigRasengan{
	constructor(x, y){
		this.x = x;
		this.y = y;
		this.speedX = 5;
		this.columnCount = 10;
		this.rowCount = 1;
		this.refreshDelay = refreshDelay; 
		this.loopColumns = true; 
		this.scale = spriteScale;
		this.sprite = new TiledImage("images/BigRasengan.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);
		this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 6);
		this.sprite.resetCol();
		this.shift = false;
	}

	tick(){
		let alive = true;
		let frame = this.sprite.getFrameCount();

		if(frame >= 5){
			this.sprite.changeMinMaxInterval(4, 4);
			this.x += this.speedX;
		} else if(frame == 4 && !this.shift){
			this.x += 80;
			this.shift = true;
		}
		if(this.x >= sasuke.x -  40){
			this.sprite.changeMinMaxInterval(5, 5);
		}
		if(this.x >= sasuke.x){
			alive = false;
		}
		this.sprite.tick(this.x, this.y, ctx);
		return alive;
	}
}