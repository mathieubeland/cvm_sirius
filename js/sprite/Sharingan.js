class Sharingan{
	constructor(){
		this.x = sasuke.x;
		this.y = spriteVerticalPosition;
		this.columnCount = 10;
		this.rowCount = 1;
		this.refreshDelay = refreshDelay * 2; 
		this.loopColumns = true; 
		this.scale = spriteScale;
		this.sprite = new TiledImage("images/sharinganAttaque.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);
		this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 10);
		this.sprite.setNextCycle(0, 0, 10, 0);
		this.sprite.resetCol();
		this.sprite.resetAfterLoop(true);
	}

	tick(){
		let alive = true;
		if(this.sprite.getFrameCount() == 7){
			alive = false;
		}
		this.x = sasuke.x;
		this.sprite.tick(this.x, this.y, ctx);
		return alive;
	}
}