class ProjectilesGenerator{
    constructor(){
        this.t = 10;
        this.generate();
        this.potentialTimeOut = 5000; //Temps de pause maximal entre le lancé des shurikens
        this.timeElapsed = true;
    }

    generate(){
        arme1 = new Shuriken(true);
        arme1.x = 0 - imagesWidth;
        arme1.y = Math.random() * height;

        arme2 = new Shuriken(false);
        arme2.x = width + imagesWidth;
        arme2.y = Math.random() * height;

        ptRencontre.x = Math.random() * width;
        ptRencontre.y = Math.random() * height;
    }

    tick(){
        if(this.timeElapsed == true){

            //Il faut recalculer la trajectoire et l'angle des armes à chaque ticks
            //Distances entre les armes et le point de recontre en utilisant pythagore
            let d1 = Math.sqrt(Math.pow(ptRencontre.x - arme1.x, 2) + Math.pow(ptRencontre.y - arme1.y, 2));
            let d2 = Math.sqrt(Math.pow(ptRencontre.x - arme2.x, 2) + Math.pow(ptRencontre.y - arme2.y, 2));

            //Vecteur unitaire décrivante la direction
            let u1 = {
                "x":null,
                "y":null
            }
            let u2 = {
                "x":null,
                "y":null
            }
            u1.x = (ptRencontre.x - arme1.x)/d1;
            u1.y = (ptRencontre.y - arme1.y)/d1;
            u2.x = (ptRencontre.x - arme2.x)/d2;
            u2.y = (ptRencontre.y - arme2.y)/d2;

            //La vitesse des deux projectiles
            let v1 = d1/this.t;
            let v2 = d2/this.t;

            //La nouvelle position des armes
            arme1.x = arme1.x + u1.x *  v1;
            arme1.y = arme1.y + u1.y * v1;
            arme2.x = arme2.x + u2.x * v2;
            arme2.y = arme2.y + u2.y * v2;

            //On dessine les armes avant l'explosion pour que l'explosion soit devant
            arme1.tick();
            arme2.tick();

            //Génère une explosion lorsque les armes se touchent
            if(Math.abs(arme1.x-arme2.x) < imagesWidth){
                ctx.drawImage(explosionImg, ptRencontre.x - (explosionImg.width/2), ptRencontre.y - (explosionImg.height/2));
            }
            
            if(Math.abs(arme1.x-arme2.x) < 8){
                this.generate();
                this.timeElapsed = false;
                setTimeout(() => {
                    this.timeElapsed = true;
                }, Math.random() * this.potentialTimeOut)
            }
        }
    }
}