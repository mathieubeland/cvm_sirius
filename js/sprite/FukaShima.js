class FukaShima extends Personnage{
	constructor(name, hp, max){
		super((positionHorizontaleFukasakuShima), (positionVerticaleFukasakuShima), 100, 100, name, hp, max);
		this.name = name;
		this.columnCount = 14;
		this.rowCount = 4;
		this.refreshDelay = refreshDelay; 
		this.loopColumns = true; 
        this.scale = spriteScale;
		this.sprite = new TiledImage("images/FukaShima.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);        
        this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 4); 
		this.sprite.setNextCycle(0, 0, 4, 0);
		this.alive = true;

		this.isAttacking = false;
		this.isMovingForward = false;
		this.isBodySlamming = false;
		this.isComingBack = false;
		this.isAppearing = true;
	}

	apparition(){
		this.sprite.changeRow(3);
		this.sprite.changeMinMaxInterval(0, 2);
		if(this.sprite.getFrameCount() > 6){
			this.isAppearing = false;
		}
	}
	
	stance(){
		this.sprite.setFlipped(false);
        this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 4); 
	}

	bodySlam(){
		this.isKaton = true;
		this.sprite.changeRow(2);
		this.sprite.changeMinMaxInterval(0, 10);
		this.sprite.resetCol();
		this.sprite.resetAfterLoop(true);
		this.actionProcess();
	}


	moveForward(){
		this.isMovingForward = true;
		this.sprite.changeRow(1);
		this.sprite.changeMinMaxInterval(0, 4);
		if(this.x >= sasuke.x - 60){
			this.isMovingForward = false;
			this.bodySlam();
			document.getElementById('fuka').play();	
		} else {
			this.x += this.speedX;
		}
	}

	comeBack(){
		this.isComingBack = true;
		this.sprite.setFlipped(true);
		this.sprite.changeRow(1);
		this.sprite.changeMinMaxInterval(0, 4);
		if(this.x >= positionHorizontaleFukasakuShima){
			this.x -= this.speedX;
		} else {
			attaqueFukasakuShima = false;
			this.isComingBack = false;
			this.stance();
		}
	}
	
	actionProcess(){		
		if(!this.isAttacking){
			this.isAttacking = true;
		}		
		if(!this.sprite.isLooping()){
			this.isAttacking = false;
			if(this.isKaton){
				this.isKaton = false;
				this.offSpring = false;
				this.comeBack();
			}
		}
	}

	isAlive(players){
		let alive = false;
		for(const iterator of players){		
			if(iterator.name == this.name){
				alive = true;
			}
		}
		if(alive == false){
			fukasakuShima = null;
		}
		this.alive = alive;
	}

	tick(){
		super.drawLifeBar(this.hp, this.max);
		if(attaqueFukasakuShima){
			if(!this.isAttacking){
				if(this.isBodySlamming){
					this.bodySlam();
				} else if(this.isComingBack){
					this.comeBack();
				} else{
					this.moveForward();
				}
			} else {
				this.actionProcess();
			}
		} else if(this.isAttacking){
			this.actionProcess();	
		}  else if(this.isAppearing){
			this.apparition();	
		} else {
			this.stance();
		}
		this.sprite.tick(this.x, this.y, ctx);
		return this.alive;
	}
}