class Naruto extends Personnage{
	constructor(){
		super((positionNaruto), (spriteVerticalPosition), 140, 100);
		this.columnCount = 12;
		this.rowCount = 6;
		this.refreshDelay = refreshDelay; 
		this.loopColumns = true; 
        this.scale = spriteScale;
		this.sprite = new TiledImage("images/naruto.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);        
        this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 4); 
		this.sprite.setNextCycle(0, 0, 4, 0);

		this.isAttacking = false;
		this.isMovingForward = false;
		this.isKicking = false;
		this.isComingBack = false;
	}

	stance(){
		this.sprite.setFlipped(false);
        this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 4); 
	}

	kick(){ //Une partie de l'attaque 1

		this.isKicking = true;
		this.sprite.changeRow(3);
		this.sprite.changeMinMaxInterval(0, 7);
		this.sprite.resetCol();
		this.sprite.resetAfterLoop(true);
		this.actionProcess();
	}

	moveForward(){
		this.isMovingForward = true;
		this.sprite.changeRow(1);
		this.sprite.changeMinMaxInterval(0, 5);
		if(this.x + this.width/2 >= sasuke.x){
			this.isMovingForward = false;
			this.kick();
			document.getElementById('tebayo').play();
		} else {
			this.x += this.speedX;
		}
	}

	comeBack(){
		this.isComingBack = true;
		this.sprite.setFlipped(true);
		this.sprite.changeRow(1);
		this.sprite.changeMinMaxInterval(0, 5);
		if(this.x >= positionNaruto){
			this.x -= this.speedX;
		} else {
			attack1 = false;
			this.isComingBack = false;
			this.stance();
		}
	}

	attack2(){
		attack2 = false;
		this.sprite.changeRow(2);
		this.sprite.changeMinMaxInterval(0, 9);
		this.sprite.resetCol();
		this.sprite.resetAfterLoop(true);
		spriteList.push(new Rasengan(this.x-15, this.y+5));
		document.getElementById('rasengan').play();
		this.actionProcess();
	}

	attack3(){
		attack3 = false;
		this.sprite.changeRow(4);
		this.sprite.changeMinMaxInterval(0, 12);
		this.sprite.resetCol();
		this.sprite.resetAfterLoop(true);
		document.getElementById('chakra').play();		
		spriteList.push(new ChakraBlast(this.x + this.width, this.y + 20, 1));
		this.actionProcess();
	}
	
	actionProcess(){		
		if(!this.isAttacking){
			this.isAttacking = true;
		}		
		if(!this.sprite.isLooping()){
			this.isAttacking = false;
			if(this.isKicking){
				this.isKicking = false;
				this.comeBack();
			}
		}
	}

	tick(){
		let alive = true;
		super.tick();

		if(attack1){
			if(!this.isAttacking){
				if(this.isKicking){
					this.kick();
				} else if(this.isComingBack){
					this.comeBack();
				} else{
					this.moveForward();
				}
			} else {
				this.actionProcess();
			}
		} else if(attack2) {
			this.attack2();
		} else if(attack3) {
			this.attack3();
		} else if(this.isAttacking){
			this.actionProcess();	
		} else{
			this.stance();
		}
		this.sprite.tick(this.x, this.y, ctx);
		return alive;
	}
}