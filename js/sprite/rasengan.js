//2e attaque de Naruto
class Rasengan{
	constructor(x, y){
		this.x = x;
		this.y = y;
		this.speedX = 5;
		this.columnCount = 10;
		this.rowCount = 1;
		this.refreshDelay = refreshDelay; 
		this.loopColumns = true; 
		this.scale = spriteScale;
		this.sprite = new TiledImage("images/rasengan.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);
		this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 7);
		this.sprite.resetCol();
		this.shift1 = false;
		this.shift2 = false;
	}

	tick(){
		let alive = true;
		let frame = this.sprite.getFrameCount();
		if(frame >= 7){
			this.sprite.changeMinMaxInterval(6, 6);
			this.x += this.speedX;
		} else if(frame == 5 && !this.shift1){
			this.x -= 20;
			this.y -= 10;
			this.shift1 = true;
		} else if(frame == 7 && !this.shift2){
			this.x += 100;
			this.shift2 = true;
		}
		if(this.x >= sasuke.x){
			alive = false;
		}

		this.sprite.tick(this.x, this.y, ctx);
		return alive;
	}
}