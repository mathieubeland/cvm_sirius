class Shuriken {
    constructor(){
		this.columnCount = 6; //La grandeur de chacune des grilles
		this.rowCount = 1;
		this.refreshDelay = 40;
		this.loopColumns = true;
        this.scale = 1.0;
		this.sprite = new TiledImage("images/shuriken.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);        
        this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(1, 5); 
		this.sprite.setFlipped(true);
	}

	tick(){
        this.sprite.tick(this.x, this.y, ctx);
    }
}