//Affiche la fin de la partie
class EndGame{
	constructor(victory){
		this.victory = victory;
		this.img = new Image();
		if(this.victory){
			this.img.src = "images/victory.png";
		} else {
			this.img.src = "images/gameOver.png";
		}
	}

	tick(){
		let alive = true;
		ctx.drawImage(this.img, 300, 400, this.img.width, this.img.height);
		return alive;
	}
}