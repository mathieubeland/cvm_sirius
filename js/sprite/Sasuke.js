class Sasuke extends Personnage{
	constructor(){
		super((positionSasuke), (spriteVerticalPosition), 140, 100);
		this.columnCount = 10;
		this.rowCount = 3;
		this.refreshDelay = refreshDelay;
		this.loopColumns = true;
        this.scale = spriteScale;
		this.sprite = new TiledImage("images/sasuke.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);        
        this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 6);
		this.sprite.setNextCycle(0, 0, 6, 0);
		this.sprite.setFlipped(true);
		
		this.isAttacking = false;
		this.isMovingForward = false;
		this.isKaton= false;
		this.isComingBack = false;
		this.offSpring = false;
	}

	stance(){
		this.sprite.setFlipped(true);
        this.sprite.changeRow(0);
		this.sprite.changeMinMaxInterval(0, 6); 
	}

	katon(){
		this.isKaton = true;
		this.sprite.changeRow(2);
		this.sprite.changeMinMaxInterval(0, 7);
		this.sprite.resetCol();
		this.sprite.resetAfterLoop(true);
		this.actionProcess();
	}


	moveForward(){
		this.isMovingForward = true;
		this.sprite.changeRow(1);
		this.sprite.changeMinMaxInterval(0, 6);
		if(this.x <= naruto.x + 80){
			this.isMovingForward = false;
			this.katon();	
			document.getElementById('katon').play();	
		} else {
			this.x -= this.speedX;
		}
	}

	comeBack(){
		this.isComingBack = true;
		this.sprite.setFlipped(false);
		this.sprite.changeRow(1);
		this.sprite.changeMinMaxInterval(0, 5);
		if(this.x <= positionSasuke){
			this.x += this.speedX;
		} else {
			attaqueSasuke = false;
			this.isComingBack = false;
			this.stance();
		}
	}
	
	actionProcess(){		
		if(!this.isAttacking){
			this.isAttacking = true;
		}		
		if(this.sprite.getCurrentCol() == 4 && !this.offSpring){
			this.offSpring = true;
			spriteList.push(new Katon(this.x - this.width/1.75, this.y-20));
		}
		if(!this.sprite.isLooping()){
			this.isAttacking = false;
			if(this.isKaton){
				this.isKaton = false;
				this.offSpring = false;
				this.comeBack();
			}
		}
	}

	tick(){
		let alive = true;
		super.tick();

		if(attaqueSasuke){
			if(!this.isAttacking){
				if(this.isKaton){
					this.katon();
				} else if(this.isComingBack){
					this.comeBack();
				} else{
					this.moveForward();
				}
			} else {
				this.actionProcess();
			}
		} else if(this.isAttacking){
			this.actionProcess();	
		} else{
			this.stance();
		}

		this.sprite.tick(this.x, this.y, ctx);
		return alive;
	}
}