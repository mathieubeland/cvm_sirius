<?php
    require_once("action/indexAction.php");
    $action = new IndexAction();
    $action->execute();
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/index.css">
    <script src="js/indexAnimation.js"></script>
    <script src="js/sprite/TiledImage.js"></script>
    <script src="js/sprite/ProjectilesGenerator.js"></script>
    <script src="js/sprite/Shuriken.js"></script>
    <audio src="audio/accueil.mp3" id="player"></audio>
    <title>Sirius</title>
</head>
<body>
    <canvas id="canvas"></canvas>
    <div>
        <div id="log_in">
            <form id = "form_login" action="index.php" method="post" onsubmit="saveUserName()">
                <?php 
                    if ($action->wrongLogin) {
                        ?>
                        <div class="error-div"><strong>Erreur : </strong>Connexion erronée</div>
                        <?php
                    }
                ?>
                <div>
                    <label>Nom d'utilisateur: </label><input type="text" name="username" id="username">
                </div>
                <div>
                    <label>Mot de passe: </label><input type="password" name="password" id="password">
                </div>
                <div>
                    <button type="submit">Entrer</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>